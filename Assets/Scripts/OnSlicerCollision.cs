﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSlicerCollision : MonoBehaviour
{
    GameManager gm;
    UIManager ui;
    LvlManager lvl;
    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        ui = FindObjectOfType<UIManager>();
        lvl = FindObjectOfType<LvlManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    GameObject slised;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "eat" & slised != other.gameObject & other.transform.GetComponent<MeshRenderer>().materials.Length <= 1)
        {
            lvl.currentPoints++; 
            slised = other.gameObject;
        }

        if (other.tag == "not eat" & slised != other.gameObject & other.transform.GetComponent<MeshRenderer>().materials.Length <= 1)
        {
            ui.wrong.SetActive(true);
            ui.wrong.GetComponent<Animator>().SetTrigger("bounce");
            gm.health--;
            slised = other.gameObject;
            Invoke("DisableWrong", 1f);
        }
    }

    void DisableWrong()
    {
        ui.wrong.SetActive(false);
    }
}
