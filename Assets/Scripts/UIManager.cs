﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private LvlManager lvl;
    private GameManager gm;

    public Slider progresBar;
    public GameObject h1, h2, h3, wrong;

    void Start()
    {
        lvl = GetComponent<LvlManager>();
        gm = GetComponent<GameManager>();
        progresBar.maxValue = lvl.lvls[lvl.currentLvl].finishZ;
    }

    private void Update()
    {
        HeartUpdate();

        ProgressUpdate();
    }

    private void ProgressUpdate()
    {
        progresBar.value = gm.heli.transform.position.z;
    }

    private void HeartUpdate()
    {
        if (gm.health == 3)
        {
            h1.SetActive(true);
            h2.SetActive(true);
            h3.SetActive(true);
        }
        else if (gm.health == 2)
        {
            h1.SetActive(true);
            h2.SetActive(true);
            h3.SetActive(false);
        }
        else if (gm.health == 1)
        {
            h1.SetActive(true);
            h2.SetActive(false);
            h3.SetActive(false);
        }
        else if (gm.health == 0)
        {
            h1.SetActive(false);
            h2.SetActive(false);
            h3.SetActive(false);
        }
    }


}
